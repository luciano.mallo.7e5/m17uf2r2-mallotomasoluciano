using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesController : MonoBehaviour
{

    void Update()
    {
        if (GameManager.IsLevelFinnished || !GameManager.IsPlayerAlive) 
        {
            SceneManager.LoadScene("FinishScene");
        }
        
    }
}
