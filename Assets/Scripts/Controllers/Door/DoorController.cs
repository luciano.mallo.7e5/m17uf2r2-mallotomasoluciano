using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public Sprite[] DoorSprite;
    void Update()
    {
        if (GameManager.TotalEnemies == 0)
        {
            GetComponent<SpriteRenderer>().sprite = DoorSprite[1];
            GetComponent<Collider2D>().isTrigger = true;
        }
        else 
        {
            GetComponent<SpriteRenderer>().sprite = DoorSprite[0];
            GetComponent<Collider2D>().isTrigger = false;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.tag=="Player") 
        {
            GameManager.PlayerPoints += 150;
            GameManager.IsLevelFinnished = true;
        }
    }
}
