using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementsManager : MonoBehaviour
{
    public delegate void AchievementDone();
    public static event AchievementDone AchievementCompleted;
    [SerializeField] private List<string> Achievements = new List<string>();

    private void Awake()
    {
        Achievements.Add("DefeatOneEnemy");
        Achievements.Add("CollectOneGoldCoin");
    }
    private void Update()
    {
        if (GameManager.EnemiesDefeated == 1 && Achievements.Contains("DefeatOneEnemy")) 
        {
            Achievements.Remove("DefeatOneEnemy");
            GameManager.AddAnAchievementsFinnished("DefeatOneEnemy");
            AchievementCompleted();
        }
        if (GameManager.GoldCoins == 1 && Achievements.Contains("CollectOneGoldCoin")) 
        {
            Achievements.Remove("CollectOneGoldCoin");
            GameManager.AddAnAchievementsFinnished("CollectOneGoldCoin");
            AchievementCompleted();
        }
    }
}
