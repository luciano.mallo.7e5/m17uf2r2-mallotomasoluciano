using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
   
    void Update()
    {

        GetComponent<Text>().text = GetScore();
    }

    private string GetScore()
    {    
            return "Total Score: " + GameManager.PlayerPoints.ToString();
    }
}
