using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FinnishMessage : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        GetComponent<Text>().text = GetMessage();
    }

    private string GetMessage() 
    {
        if (GameManager.IsLevelFinnished)
        {
            return "Well done";
        }

        else 
        {
            return "You Fail";
        }
    }
}
