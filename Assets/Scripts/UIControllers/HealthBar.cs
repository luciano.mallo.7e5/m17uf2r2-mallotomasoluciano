using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;
    [SerializeField]public float value;
    public void SetHealth() 
    {
        slider.value = GameManager.PlayerHealth;
        value = slider.value;
    }
    private void Update()
    {
        SetHealth();
    }
}
