using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/PlayerData", order = 1)]
public class PlayerDataSO : ScriptableObject
{

    public int Health;
    public int speed;
    public bool independentMovement;
    public bool IsAlive;
    public Animator animator;
    public ItemSo[] inventory;
    public WeaponSO[] weapons;
    public ItemSo items;



}
