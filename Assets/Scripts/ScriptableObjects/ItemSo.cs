using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/Inventory", order = 1)]
public class ItemSo : ScriptableObject
{
    public string Name;
    public Sprite ItemSprite;


}
