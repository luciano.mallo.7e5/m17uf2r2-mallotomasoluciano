using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour, IDamageble
{
    public int MaxHealth;
    public int actualHealth;
    public int speed;
    public int damage;
    public Vector3 currentPosition;
    public bool independentMovement;
    public bool IsAlive;

    public abstract void Move();
    public abstract void Attack();

    public abstract void Die();

    public void RecieveDamage(int damage)
    {
        actualHealth-=damage;
    }
}
