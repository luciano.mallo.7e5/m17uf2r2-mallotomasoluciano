using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : Enemy
{
    private Transform target;
    private float _impulseForce = 30f;
    public Animator Animator;
    private bool explode;

   
    // Start is called before the first frame update
    void Start()
    {
        this.pointsByDefeating = 50;
        this.name = "Goblin";
        this.speed = 1;
        this.actualHealth = 40;
        this.currentPosition = transform.position;
        this.damage = 20;
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Die();
    }
    public override void Attack()
    {
        throw new System.NotImplementedException();
    }
    public override void Die()
    {
        
        if (actualHealth <= 0 || explode)
        {
            base.Die();
            Destroy(gameObject);
        }
    }

    public override void Move()
    {

        FollowPlayer();

    }
    public void FollowPlayer() 
    {

        transform.position = Vector2.MoveTowards(transform.position,target.position,speed * Time.deltaTime);
        Animator.SetFloat("X", -transform.position.x);
        Animator.SetFloat("Y", transform.position.y);
    }
   

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" || collision.collider.tag == "Bullet")
        {
            if (collision.collider.tag == "Player")
            {

                GameObject PlayerGO = collision.gameObject;
                PlayerGO.GetComponent<IDamageble>().RecieveDamage(damage);
                Vector2 direction = (Vector2)PlayerGO.transform.position - (Vector2)transform.position;
                PlayerGO.GetComponent<Rigidbody2D>().AddForce(direction * _impulseForce, ForceMode2D.Impulse);
                explode = true;
                Die();
            }


        }
    }


}
