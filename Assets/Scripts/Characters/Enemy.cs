using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    public int pointsByDefeating;
    private void Awake()
    {
        GameManager.TotalEnemies++;
    }
    public override void Attack(){}

    public override void Die()
    {
        GameManager.PlayerPoints += pointsByDefeating;
        GameManager.TotalEnemies--;
        GameManager.EnemiesDefeated++;
    }
    public override void Move(){}

    // public override void ReceiveDamage(int damage){}

    
}
