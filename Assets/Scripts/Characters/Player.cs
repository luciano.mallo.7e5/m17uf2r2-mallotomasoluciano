using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;



public class Player : Character
{


    public Animator animator;
    [SerializeField] private int numOfWeapon;
    [SerializeField] private ItemSo[] inventory;
    public WeaponSO[] weapons = new WeaponSO[5];
    public PlayerDataSO playerData;
    [SerializeField] private int TotalEnemies;
    private bool _isMoving;
    public List<string> Achievements;




    private void Awake()
    {
        GameManager.PlayerCurrentPosition = transform.position;

        SaveInstanceOFPlayerDataBase();
        LoadActualPlayerData();
        LoadFirstWeapon();
        actualHealth = MaxHealth;
        numOfWeapon = 0;
        this.independentMovement = false;
        GameManager.IsPlayerAlive = IsAlive;
        GameManager.PlayerHealth = actualHealth;
        Achievements = GameManager.AchievementsFinnished;
    }
    void FixedUpdate()
    {
        Move();
    }
    void Update()
    {

        TotalEnemies = GameManager.TotalEnemies;
        UpdateHealth();
        FollowPointer();
        ChangeWeapon();
        Attack();
        Reload();
        Die();


    }
    private void UpdateHealth()
    {
        GameManager.PlayerHealth = actualHealth;
    }
    private void FollowPointer()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.up = mousePos - new Vector2(transform.position.x, transform.position.y);
    }

    public override void Die()
    {
        if (actualHealth <= 0)
        {
            GameManager.IsPlayerAlive = false;

        }
    }
    public override void Move()
    {
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {

                Vector2 direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
                GetComponent<Dash>().DoDash(direction.normalized);

            }
            else
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxis("Horizontal") * speed * Time.deltaTime, Input.GetAxis("Vertical") * speed * Time.deltaTime);

            }

            if (!_isMoving)
            {
                _isMoving = true;
                animator.SetBool("IsMoving", _isMoving);

            }

        }
        else
        {
            if (_isMoving)
            {
                _isMoving = false;
                animator.SetBool("IsMoving", _isMoving);
            }
        }
    }

    public override void Attack()
    {

        if (Input.GetMouseButton(0))
        {
            Transform weaponGO = this.gameObject.transform.GetChild(0);
            weaponGO.gameObject.GetComponent<WeaponController>().Shoot();
        }
    }

    public void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Transform weaponGO = this.gameObject.transform.GetChild(0);
            weaponGO.gameObject.GetComponent<WeaponController>().Reload();
        }
    }
    public void ChangeWeapon()
    {


        if (Input.GetAxis("Mouse ScrollWheel") > 0f || Input.GetKeyDown("c")) // forward
        {

            if (numOfWeapon < 4)
            {

                Transform weaponGO = this.gameObject.transform.GetChild(0);
                weapons[numOfWeapon] = weaponGO.gameObject.GetComponent<WeaponController>().SaveStateOfWeapon(weapons[numOfWeapon]);
                numOfWeapon++;
                weaponGO.gameObject.GetComponent<WeaponController>().LoadWeapon(weapons[numOfWeapon]);

            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f || Input.GetKeyDown("x")) // backwards
        {
            if (numOfWeapon > 0)
            {

                Transform weaponGO = this.gameObject.transform.GetChild(0);
                weapons[numOfWeapon] = weaponGO.gameObject.GetComponent<WeaponController>().SaveStateOfWeapon(weapons[numOfWeapon]);
                numOfWeapon--;
                weaponGO.gameObject.GetComponent<WeaponController>().LoadWeapon(weapons[numOfWeapon]);


            }

        }
    }
    public void LoadFirstWeapon()
    {
        Transform weaponGO = this.gameObject.transform.GetChild(0);
        weaponGO.gameObject.GetComponent<WeaponController>().LoadWeapon(weapons[0]);
    }
    public void LoadActualPlayerData()
    {
        playerData = Resources.Load<PlayerDataSO>("PlayerDataActual/ActualPlayerDataSO");
        this.name = playerData.name;
        this.IsAlive = playerData.IsAlive;
        this.speed = playerData.speed;
        this.MaxHealth = playerData.Health;
        this.independentMovement = playerData.independentMovement;
        for (int i = 0; i < weapons.Length; i++)
        {
            this.weapons[i] = (WeaponSO)ScriptableObject.CreateInstance(typeof(WeaponSO));
            this.weapons[i].Init(playerData.weapons[i]);
        }

    }
    public void SaveInstanceOFPlayerDataBase()
    {
        
        string newAssetPath = "Assets/Resources/PlayerDataActual/ActualPlayerDataSO.asset";
       /* AssetDatabase.CreateAsset();
        if (AssetDatabase.CopyAsset("Assets/ScriptableObjectes/PlayerData/PlayerDataBase.asset", newAssetPath))
        { 
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }*/
    }

}
