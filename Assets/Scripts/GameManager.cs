using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static List<string> _achievementsFinnished = new List<string>();
    public static List<string> AchievementsFinnished
    {
        get => _achievementsFinnished; 
    }
    public static void AddAnAchievementsFinnished(string achievement) 
    {
        _achievementsFinnished.Add(achievement); 
    }
    
    private static bool _isLevelFinnished;
    public static bool IsLevelFinnished
    {
        get => _isLevelFinnished;
        set => _isLevelFinnished = value;
    }
    private static int _goldCoins=0;
    public static int GoldCoins
    {
        get => _goldCoins;
        set => _goldCoins = value;
    }

    private static int _totalEnemies = 0;

    public static int TotalEnemies
    {
        get => _totalEnemies;
        set => _totalEnemies = value;
    }
    private static int _enemiesDefeated = 0;

    public static int EnemiesDefeated
    {
        get => _enemiesDefeated;
        set => _enemiesDefeated = value;
    }

    private static int _playerPoints = 0;

    public static int PlayerPoints
    {
        get => _playerPoints;
        set => _playerPoints = value;
    }


    private static bool _isPlayerAlive = true;
    public static bool IsPlayerAlive
    {
        get => _isPlayerAlive;
        set => _isPlayerAlive = value;
    }

    private static int _playerHealth;
    public static int PlayerHealth
    {
        get => _playerHealth;
        set => _playerHealth = value;
    }

    private static Vector3 _playerCurrentPosition;
    public static Vector3 PlayerCurrentPosition
    {
        get => _playerCurrentPosition;
        set => _playerCurrentPosition = value;
    }
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {

            if (_instance is null)
            {
                Debug.LogError("GameManager MAnager is null");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        
        _instance = this;
      
    }
    
}
